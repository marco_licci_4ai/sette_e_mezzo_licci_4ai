package com.marcolicci.settemezzo;

import java.util.Collections;
import java.util.Stack;

/**
 * Created by marco on 22/11/2016.
 */
public class Deck extends Stack<Card> {
    Deck(){
        super();
        for (Suite suite : Suite.values()) {
            for(Value value : Value.values()){
                this.push(new Card(value, suite));
            }
        }
        Collections.shuffle(this);
    }
}
