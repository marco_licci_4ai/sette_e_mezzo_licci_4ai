package com.marcolicci.settemezzo;

/**
 * Created by marco on 22/11/2016.
 */
public enum Suite {
    HEARTS,
    DIAMONDS,
    CLUBS,
    SPADES
}
