package com.marcolicci.settemezzo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Game extends Application {

    static private Player[] players = new Player[2];
    static private int playing = 0;
    static private Deck deck = new Deck();

    public static int getPlayingId() {
        return playing;
    }

    public static Player getPlaying() {
        return players[playing];
    }

    public static List<Player> getPlayers(){
        return Collections.unmodifiableList((Arrays.asList(players)));
    }

    public static void giveCard() {
        getPlaying().addCard(deck.pop());
    }

    public static void nextTurn(){
        if (playing != -1) {
            playing++;
        }
        if(playing > 1){
            playing = -1;
        }
    }

    public static int getWinner(){
        int winnerId = -1;
        int highestScore = -1;
        for(int i = 0; i < players.length; i++) {
            if(players[i].getScore() > highestScore){
                winnerId = i;
            }
        }
        return winnerId;
    }

    public static void reset(){
        players[0] = new Player();
        players[1] = new Player();
        playing = 0;
        deck = new Deck();
    }


    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("view.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 800, 800));
        primaryStage.setMinHeight(800);
        primaryStage.setMinWidth(800);
        primaryStage.show();
    }


    public static void main(String[] args) {
        players[0] = new Player();
        players[1] = new Player();
        launch(args);
    }
}
