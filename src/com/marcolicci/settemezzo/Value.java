package com.marcolicci.settemezzo;

/**
 * Created by marco on 22/11/2016.
 */
public enum Value {
    ACE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    JACK,
    QUEEN,
    KING
}