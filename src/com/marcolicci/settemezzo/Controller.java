package com.marcolicci.settemezzo;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class Controller {
    @FXML
    private GridPane cardsTable;

    @FXML
    private Text status;

    @FXML
    private Text scoreA;

    @FXML
    private Text scoreB;

    @FXML
    public void initialize() {
        if(Game.getPlayingId() == 0)
            status.setText("Playing A");
        else if(Game.getPlayingId() == 1)
            status.setText("Playing B");

        else if(Game.getPlayingId() == -1){
            if(Game.getWinner() == -1)
                status.setText("Tie!");
            else if(Game.getWinner() == 0)
                status.setText("Player A won!");
            else if(Game.getWinner() == 1)
                status.setText("Player B won!");
        }
        scoreA.setText(Float.toString(Game.getPlayers().get(0).getScore()));
        scoreB.setText(Float.toString(Game.getPlayers().get(1).getScore()));
        displayCards();
    }

    private void displayCards(){
        Card card;
        Image cardImage;
        ImageView imageView;
        for (int i = 0; i < 14; i++) {
            try{
                card = Game.getPlaying().getCards().get(i);
                cardImage = new Image(getClass().getResourceAsStream("\\cardimages\\" +
                        card.getValue().name() + "_" + card.getSuite().name() + ".jpg"));
            } catch (IndexOutOfBoundsException e){
                cardImage = new Image(getClass().getResourceAsStream("\\cardimages\\rear.jpg"));
            }
            imageView = new ImageView(cardImage);
            imageView.setFitHeight(100);
            imageView.fitHeightProperty().bind(cardsTable.heightProperty().divide(4));
            imageView.fitWidthProperty().bind(cardsTable.widthProperty().divide(5.3));
            cardsTable.add(imageView, i%4, i/4);
        }
    }

    @FXML
    private void requestCard(){
        if(Game.getPlayingId() != -1) {
            Game.giveCard();
            initialize();
            Platform.runLater(() -> {
                if (!Game.getPlaying().canPlay()) {
                    try {
                        Thread.sleep(3000);
                    } catch (Exception ex) {
                    }
                    Game.nextTurn();
                    initialize();
                }
            });
        }
    }

    @FXML
    private void stay(){
        Game.nextTurn();
        initialize();
    }

    @FXML
    private void reset(){
        Game.reset();
        initialize();
    }
}
