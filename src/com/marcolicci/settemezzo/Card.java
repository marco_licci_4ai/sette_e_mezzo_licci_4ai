package com.marcolicci.settemezzo;

/**
 * Created by marco on 22/11/2016.
 */
public class Card {
    private final Value value;
    private final Suite suite;

    public Value getValue() {
        return value;
    }

    public Suite getSuite() {
        return suite;
    }

    public Card(Value value, Suite suite) {
        this.value = value;
        this.suite = suite;
    }
}
