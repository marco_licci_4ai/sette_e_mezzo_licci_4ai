package com.marcolicci.settemezzo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by marco on 22/11/2016.
 */
public class Player {
    private float score;
    private ArrayList<Card> cards;

    Player(){
        cards = new ArrayList<>();
    }

    public float getScore() {
        return score;
    }

    public void addCard(Card card){
        if(card.getValue().ordinal() <= 7){
            score += card.getValue().ordinal() + 1;
        }
        else {
            score += 0.5;
        }
        if(score > 7.5){
            score = -1;
        }
        cards.add(card);
    }

    public List<Card> getCards(){
        return Collections.unmodifiableList((cards));
    }

    public boolean canPlay(){
        return !(score == 7.5 || score == -1);
    }
}
